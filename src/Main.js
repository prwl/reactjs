import React, { Component } from 'react';
import { Route, Switch } from 'react-router-dom';

import { Hello, HelloWorld }  from './Components/Class/Hello';
import { JsxExample, Jsx } from './Components/Jsx/Jsx';
import { Rendering, RenderingExample } from './Components//Rendering/Rendering';
import { Functional, Welcome } from './Components/Functional/Functional';
import { Toggle, ToggleExample } from './Components/Handling/Handling';
import { GreetingExample, Greeting } from './Components/Conditional/Conditional';
import { LoginControl, LoginControlExample } from './Components/Conditional/Conditional-Part2';
import { WarningBannerExample, Page } from './Components/Conditional/Conditional-Part3';
import { NumberList , NumberListExample } from './Components/List/List';
import { NumberKeyExample, NumberKey } from './Components/Keys/Keys';
import { BlogExample, Blog } from './Components/Keys/Keys-Part2';
import { Form, FormExample } from './Components/Form/Form';
import { NameForm, NameFormExample } from './Components/Controlled/Controlled';
import { Calculator, CalculatorExample } from './Components/Lifting/Lifting';
import { WelcomeDialog, WelcomeDialogExample } from './Components/Composition/Composition';
import { App, AppExample } from './Components/Composition/Composition-Part2';
import { SignUpDialog, SignUpDialogExample } from './Components/Composition/Composition-Part3';
import Router from './Components/Router/Router';
import Intro from './Components/Intro/Intro';

class View extends Component{
    render(){
        return(
                <div className="card text-center">
                        <div className="card-header">
                
                        </div>
                        <div className="card-block">
                            <Switch>
                                <Route path="/class" component={Hello}/>
                                <Route path="/jsx" component={JsxExample}/>
                                <Route path="/rendering-elements" component={Rendering}/>
                                <Route path="/functional" component={Functional}/>
                                <Route path="/handling" component={Toggle}/>
                                <Route path="/conditional" component={Greeting}/>
                                <Route path="/conditional-part2" component={LoginControl} />
                                <Route path="/conditional-part3" component={Page} />
                                <Route path="/list" component={NumberList}/>
                                <Route path="/list-example" render={ (props) => <NumberListExample numbers={[1, 2, 3, 4, 5]} />}/>
                                <Route path="/keys" component={NumberKey}/>
                                <Route path="/keys-example" render={ (props) => <NumberKeyExample numbers={[100, 200, 300, 400, 500]} />}/>
                                <Route path="/keys-example-part2" 
                                    render={
                                        (props) => 
                                                <BlogExample posts={[
                                                    {id: 1, title: 'Hello World', content: 'Welcome to learning React!'},
                                                    {id: 2, title: 'Installation', content: 'You can install React from npm.'}
                                                ]}
                                                />
                                    }
                                />
                                <Route path="/keys-part2" component={Blog}/>
                                <Route exact={true} path="/" component={Intro}/>
                                <Route path="/form" component={Form}/>
                                <Route path="/form-example" component={FormExample}/>
                                <Route path="/controlled-components" component={NameForm}/>
                                <Route path="/controlled-components-example" component={NameFormExample}/>
                                <Route path="/lifting" component={Calculator}/>
                                <Route path="/lifting-example" component={CalculatorExample}/>
                                <Route path="/composition" component={WelcomeDialog}/>
                                <Route path="/composition-example" component={WelcomeDialogExample}/>
                                <Route path="/composition-part2" component={App}/>
                                <Route path="/composition-part2-example" component={AppExample}/>
                                <Route path="/composition-part3" component={SignUpDialog}/>
                                <Route path="/composition-part3-example" component={SignUpDialogExample}/>
                                <Route path="/router" component={Router}/>
                                <Route path="/hello-world" component={HelloWorld}/> 
                                <Route path="/welcome" render={ (props) => <Welcome name="King" />}/> 
                                <Route path="/jsx-example" component={Jsx}/> 
                                <Route path="/rendering-element-example" component={RenderingExample}/> 
                                <Route path="/toggle-example" component={ToggleExample}/> 
                                <Route path="/conditional-example-part1" render={ (props) => <GreetingExample isLoggedIn={false} />}/>
                                <Route path="/conditional-example-part2" component={LoginControlExample} />
                                <Route path="/conditional-example-part3" component={WarningBannerExample} />
                            </Switch>
                        </div> 
                                <div className="card-footer text-muted">
                                </div>
                        </div>
                );
    }
}

export default View;