import React, { Component } from 'react';

// Component to display the UI
class Intro extends Component {
    render() {
        return (
                <div>
                    <div className="card text-center">
                        <div className="card-block">
                            <h4 className="card-title text-muted">React Js</h4>
                            <p className="card-text">
                                <span className="text-primary"><b>* What ???</b></span>
                                <br /><span className="text-success"><b>* Why ???</b></span>
                                <br /><span className="text-danger"><b>* and How ???</b></span></p>
                        </div>
                    </div>
                </div>
                );
    }
}

export default Intro;