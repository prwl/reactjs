import React, { Component } from 'react';
import { Link } from 'react-router-dom';

// Main component
class RenderingExample extends Component {
    constructor(props) {
        super(props);
        this.state = {date: new Date()};
    }

    componentDidMount() {
        this.timerID = setInterval(
                () => this.tick(),
                1000
                );
    }

    componentWillUnmount() {
        clearInterval(this.timerID);
    }

    tick() {
        this.setState({
            date: new Date()
        });
    }

    render() {
        return (
                <div>
                    <h1>Good Evening!</h1>
                    <h2>It is {this.state.date.toLocaleTimeString()}.</h2>
                </div>
                );
    }
}

// Component to dispay the UI
class Rendering extends Component {
    render() {
        return (
                <div>
                    <div className="card text-left">
                        <div className="card-block">
                            <h4 className="card-title text-muted">Rendering Elements</h4>
                            <p className="card-text">Elements are the smallest building blocks of React apps.<br />
                                * React elements are immutable. Once you create an element, you can’t change its children or attributes.
                                <br />* An element is like a single frame in a movie: it represents the UI at a certain point in time.</p>
                            <blockquote className='blockquote bg-warning text-white'>
                                Note:
                                <br />One might confuse elements with a more widely known concept of “components”. We will introduce components in the next section. Elements are what components are “made of”.</blockquote>
                            <Link className="btn btn-success text-white card-link" to="/rendering-element-example">Example</Link>  
                        </div>
                    </div>
                </div>
                );
    }
}

export {
Rendering,
        RenderingExample,
}