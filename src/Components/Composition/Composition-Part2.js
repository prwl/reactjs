import React from 'react';
import { Link } from 'react-router-dom';

// Main Component
function AppExample() {
    return (
    <SplitPane left={ <Contacts /> } right={ <Chat /> } />
  );
}

// Component rendered in the main component
function SplitPane(props) {
  return (
    <div className="SplitPane">
      <div className="SplitPane-left">
        {props.left}
      </div>
      <div className="SplitPane-right">
        {props.right}
      </div>
    </div>
  );
}

// Component passed as a prop in the main component
function Contacts() {
    return <div className="Contacts text-white"> <br /><br />Left</div>;
}

// Component passed as a prop in the main component
function Chat() {
  return <div className="Chat text-white"> <br /><br />Right</div>;
}

// Component to render the UI
function App() {
    return(
            <div>
                    <div className="card text-left">
                        <div className="card-block">
                            <p className="card-text">
                            * While using <b><i>children</i></b> props is less common, sometimes you might need multiple “holes” in a component. 
                                <br />* In such cases you may come up with your own convention instead of using children</p>
                            <Link className="btn btn-success text-white card-link" to="/composition-part2-example">Example</Link>  
                        </div>
                    </div>
                </div>
            );
}

export {
    App,
    AppExample
}