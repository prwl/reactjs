import React from 'react';
import { Link } from 'react-router-dom';

// Main component 
function WelcomeDialogExample() {
    return (
            <FancyBorder color="blue">
                <h1 className="Dialog-title">
                    Welcome
                </h1>
                <p className="Dialog-message">
                    Today is a fine & pleasant weather!
                </p>
            </FancyBorder>
            );
}

// Component to render all the things passed as a children prop
function FancyBorder(props) {
    return (
            <div className={'FancyBorder FancyBorder-' + props.color}>
                {props.children}
            </div>
            );
}

// Component to render the main UI
function WelcomeDialog() {
    return (
            <div>
                <div className="card text-left">
                    <div className="card-block">
                        <h4 className="card-title text-muted">Composition vs Inheritance</h4>
                        <p className="card-text">React has a powerful composition model, and we recommend using composition instead of inheritance to reuse code between components.<br /></p>
                        <h5 className="card-subtitle mb-2 text-muted">Containment</h5>
                        * Some components don’t know their children ahead of time. This is especially common for components like Sidebar or Dialog that represent generic “boxes”.
                        <br />* We recommend that such components use the special children prop to pass children elements directly into their output.<br /><br />
                        <Link className="btn btn-success text-white card-link" to="/composition-example">Example</Link>  
                    </div>
                </div>
            </div>
            );
}

export {
WelcomeDialog,
        WelcomeDialogExample
}