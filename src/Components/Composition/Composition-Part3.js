import React, { Component } from 'react';
import { Link } from 'react-router-dom';

// Main Component
class SignUpDialogExample extends Component {
    constructor(props) {
        super(props);
        this.handleChange = this.handleChange.bind(this);
        this.handleSignUp = this.handleSignUp.bind(this);
        this.state = {login: ''};
    }

    render() {
        return (
                <Dialog title="Mars Exploration Program"
                        message="How should we refer to you?">
                    <input value={this.state.login}
                           onChange={this.handleChange} />
                    <button className="btn btn-sm btn-warning" onClick={this.handleSignUp}>
                        Sign Me Up!
                    </button>
                </Dialog>
                );
    }

    // Method to set the value in the input box field 
    handleChange(e) {
        this.setState({login: e.target.value});
    }

    // Method to show the alert message with the text entered in the input text box
    handleSignUp() {
        alert(`Welcome aboard, ${this.state.login}!`);
    }
}

// Component that renders the various things passed as props from the SignUpDialogExample component
function Dialog(props) {
    return (
            <FancyBorder color="blue">
                <h1 className="Dialog-title">
                    {props.title}
                </h1>
                <p className="Dialog-message">
                    {props.message}
                </p>
                {props.children}
            </FancyBorder>
            );
}

// Component to generate blue border
function FancyBorder(props) {
    return (
            <div className={'FancyBorder FancyBorder-' + props.color}>
                {props.children}
            </div>
            );
}

// Component to render the UI
class SignUpDialog extends Component {
    render() {
        return(
                <div>
                    <div className="card text-left">
                        <div className="card-block">
                            <h4 className="card-title text-muted">Specialization</h4>
                            <p className="card-text">
                                * Sometimes we think about components as being “special cases” of other components.
                                <br />* For example, we might say that a WelcomeDialog is a special case of Dialog.
                                <br />* In React, this is also achieved by composition, where a more “specific” component renders a more “generic” one and configures it with props.
                                <br />* Props and composition give you all the flexibility you need to customize a component’s look and behavior in an explicit and safe way.</p>
                            <Link className="btn btn-success text-white card-link" to="/composition-part3-example">Example</Link>  
                        </div>
                    </div>
                </div>
                );
    }
}

export {
SignUpDialog,
        SignUpDialogExample
        }