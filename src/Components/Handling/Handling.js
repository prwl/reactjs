import React, { Component } from 'react';
import { Link } from 'react-router-dom';

// Main component
class ToggleExample extends Component {
    constructor(props) {
        super(props);
        this.state = {isToggleOn: true};

        // This binding is necessary to make `this` work in the callback
        this.handleClick = this.handleClick.bind(this);
    }

    handleClick() {
        this.setState(prevState => ({
                isToggleOn: !prevState.isToggleOn
            }));
    }

    render() {
        return (
                <div>
                    <span className="text-danger">Toggling Button Example:</span><br /><br />
                    <button className="btn btn-info" onClick={this.handleClick}>
                        {this.state.isToggleOn ? 'ON' : 'OFF'}
                    </button>
                </div>
                );
    }
}

// Component to render the UI
class Toggle extends Component {
    render() {
        return(
                <div>
                    <div className="card text-left">
                        <div className="card-block">
                            <h4 className="card-title text-muted">Handling Events</h4>
                            <p className="card-text">React events are named using camelCase.<br />
                                <br />* With JSX you pass a function as the event handler, rather than a string.
                                <br />* Another difference is that you cannot return false to prevent default behavior in React. You must call preventDefault explicitly.
                                <br />* For example:
                                <br />&nbsp;&nbsp;&nbsp;{'<button onclick="activateLasers()">'} is in <b><i>javascript</i></b> and,
                                <br />&nbsp;&nbsp;&nbsp;{'<button onClick={activateLasers}>'} is in <b><i>react</i></b>.
                                <br /><br />&nbsp;&nbsp;&nbsp;{'<a href="#" onclick="console.log("The link was clicked."); return false">'} you cannot return false to prevent default behavior in React like this in Js.
                                <br />* Generally, if you refer to a method without () after it, such as {'onClick={this.handleClick}'}, you should bind that method.
                                <br />&nbsp;&nbsp;&nbsp;If you forget to bind and pass it to any event, this will be undefined when the function is actually called.</p>
                            <Link className="btn btn-success text-white card-link" to="/toggle-example">Example</Link>  
                        </div>
                    </div>
                </div>
                );
    }
}

export {
Toggle,
        ToggleExample,
}