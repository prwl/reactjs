import React, { Component } from 'react';
import { Link } from 'react-router-dom';

// Main component
class NameFormExample extends Component {
    constructor(props) {
        super(props);
        this.state = {value: '',
            valueTextarea: '',
            valueSelect: 'Coconut'};

        this.handleChange = this.handleChange.bind(this);
        this.handleChangeTextarea = this.handleChangeTextarea.bind(this);
        this.handleChangeSelect = this.handleChangeSelect.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    // Method to set state of input text box field from the user input
    handleChange(event) {
        this.setState({value: event.target.value});
    }

    // Method to set state of text area field from the user input
    handleChangeTextarea(event) {
        this.setState({valueTextarea: event.target.value});
    }

    // Method to set state of select dropdown from the user input
    handleChangeSelect(event) {
        this.setState({valueSelect: event.target.value});
    }

    // Method to prevent default submit behaviour of submit button and display all the values in the alert box when button pressed  
    handleSubmit(event) {
        alert('Name: ' + this.state.value + ' \nEssay: ' + this.state.valueTextarea + ' \nFlavor: ' + this.state.valueSelect);
        event.preventDefault();
    }

    render() {
        return (
                <div className="container">
                    <form onSubmit={this.handleSubmit}>
                        <div className="form-group row">
                            <label className="col-sm-2 col-form-label">
                                Name:
                            </label>
                            <input className="col-sm-2 form-control" type="text" value={this.state.value} onChange={this.handleChange} />
                        </div>
                        <div className="form-group row">
                            <label className="col-sm-2 col-form-label">
                                Essay:
                            </label>
                            <textarea className="col-sm-2 form-control" value={this.state.valueTextarea} onChange={this.handleChangeTextarea} />
                        </div>
                        <div className="form-group row">
                            <label className="col-sm-2 col-form-label">
                                Soda flavor:
                            </label>
                            <select className="col-sm-2 form-control" value={this.state.valueSelect} onChange={this.handleChangeSelect}>
                                <option value="Grapefruit">Grapefruit</option>
                                <option value="Lime">Lime</option>
                                <option value="Coconut">Coconut</option>
                                <option value="Mango">Mango</option>
                            </select>
                        </div>
                        <div className="form-group row">
                            <div className="col-sm-2"></div>
                            <input className="btn btn-success" type="submit" value="Submit" />
                        </div>
                    </form>
                </div>
                );
    }
}

// Component to render the UI
class NameForm extends Component {
    render() {
        return(
                <div>
                    <div className="card text-left">
                        <div className="card-block">
                            <h4 className="card-title text-muted">Controlled Components</h4>
                            <p className="card-text">
                                * In HTML, form elements such as &lt;input&gt;, &lt;textarea&gt;, and &lt;select&gt; typically maintain their own state and update it based on user input.
                                <br />* In React, mutable state is typically kept in the state property of components, and only updated with setState().
                                <br />* We can combine the two by making the React state be the “<b><i>single source of truth</i></b>”. 
                                <br />* Then the React component that renders a form also controls what happens in that form on subsequent user input.
                                <br />* An input form element whose value is controlled by React in this way is called a “<b><i>controlled component</i></b>”.
                                <br />* Since the value attribute is set on our form element, the displayed value will always be this.state.value, making the React state the source of truth.
                                <br />* Since handleChange runs on every keystroke to update the React state, the displayed value will update as the user types.
                                <br />* With a controlled component, every state mutation will have an associated handler function. This makes it straightforward to modify or validate user input.
                                <br />* <b>For example</b>, 
                                <br />&nbsp;&nbsp;&nbsp;if we wanted to enforce that names are written with all uppercase letters, we could write handleChange as:
                                <br />&nbsp;&nbsp;&nbsp;<i>{ ' this.setState({value: event.target.value.toUpperCase()});' }</i></p>
                            <Link className="btn btn-success text-white card-link" to="/controlled-components-example">Example</Link>  
                        </div>
                    </div>
                </div>
                );
    }
}

export {
NameForm,
        NameFormExample
        }