import React, { Component } from 'react';
import { Link } from 'react-router-dom';

// Main class component
class HelloWorld extends Component {
    render() {
        return (
                <div className='text-left'>
                    Hello World!
                </div>
                );
    }
}

// Component to display the UI
class Hello extends Component {
    render() {
        return (
                <div>
                    <div className="card text-left">
                        <div className="card-block">
                            <h4 className="card-title text-muted">Class Components</h4>
                            <p className="card-text">Classes have some additional features.<br />
                                <br />* Local state is exactly that: a feature available only to classes.
                                <br /><b>* <i>State</i></b> is similar to props, but it is private and fully controlled by the component.</p>
                            <h5 className="card-subtitle mb-2 text-muted">Converting a Function to a Class</h5>
                            You can convert a functional component to a class in these steps:<br />
                
                            <br />1. Create an ES6 class, with the same name, that extends React.Component.
                            <br />2. Add a single empty method to it called render().
                            <br />3. Move the body of the function into the render() method.
                            <br />4. Replace props with this.props in the render() body.<br /><br />
                            <Link className="btn btn-success text-white card-link" to="/hello-world">Example</Link>  
                        </div>
                    </div>
                </div>
                );
    }
}

export { // without default
        Hello,
        HelloWorld,
}