import React from 'react';
import { Link } from 'react-router-dom';

// Main component
function NumberListExample(props) {
    const liStyle = {
        listStylePosition: "inside",
    }
    const numbers = props.numbers;
    const listItems = numbers.map((number) =>
        <li>{number}</li>
    );
    return (
            <ul style={liStyle}>{listItems}</ul>
            );
}

// Component to render the UI
function NumberList(props) {
    return(
            <div>
                <div className="card text-left">
                    <div className="card-block">
                        <h4 className="card-title text-muted">List</h4>
                        <p className="card-text">
                            * we use the map() function to take an array of numbers and double their values.
                            <br />* You can build collections of elements and include them in JSX using curly braces {}.
                            <br />* For Example: 
                            <br />&nbsp;&nbsp;&nbsp;The example below displays a bullet list of numbers between 1 and 5.
                            <br />* When you run this example, you’ll be given a warning that a key should be provided for list items. we will talk about it in the next section.</p>
                        <Link className="btn btn-success text-white card-link" to="/list-example">Example</Link>  
                    </div>
                </div>
            </div>
            );
}

export { NumberList,
        NumberListExample
}