import React, { Component } from 'react';
import { Link } from 'react-router-dom';

// Main component
class FormExample extends Component {
    render() {
        return (
                <form>
                    <div className="form-group row">
                        <label className="col-sm-2 col-form-label">
                            Name:
                        </label>
                        <input className="col-sm-2 form-control" type="text" name="name" />
                    </div>
                    <div className="form-group row">
                        <div className="col-sm-2"></div>
                        <input className="btn btn-info" type="submit" value="Submit" />
                    </div>
                </form>
                );
    }
}

// Component to render the UI
class Form extends Component {
    render() {
        return (
                <div>
                    <div className="card text-left">
                        <div className="card-block">
                            <h4 className="card-title text-muted">Forms</h4>
                            <p className="card-text">
                                * The form has the default HTML form behavior of browsing to a new page when the user submits the form. If you want this behavior in React, it just works.
                                <br />* But in most cases, it’s convenient to have a JavaScript function that handles the submission of the form and has access to the data that the user entered into the form.
                                <br />* The standard way to achieve this is with a technique called “<b><i>controlled components</i></b>”.</p>
                            <Link className="btn btn-success text-white card-link" to="/form-example">Example</Link>  
                        </div>
                    </div>
                </div>
                );
    }
}

export { 
    Form,
    FormExample
}