import React from 'react';
import { Link } from 'react-router-dom';

// Main Component
function GreetingExample(props) {
    const isLoggedIn = props.isLoggedIn;
    if (isLoggedIn) {
        return <UserGreeting />;
    }
    return <GuestGreeting />;
}

// Component for the logged in user
function UserGreeting(props) {
    return <h1>Welcome back!</h1>;
}

// Component for the guest user
function GuestGreeting(props) {
    return <h1>Please sign up.</h1>;
}

// Component to render the UI
function Greeting(props) {
    return (
            <div>
                <div className="card text-left">
                    <div className="card-block">
                        <h4 className="card-title text-muted">Conditional Rendering</h4>
                        <p className="card-text">Conditional rendering in React works the same way conditions work in JavaScript.
                            <br />Use JavaScript operators like if or the conditional operator to create elements representing the current state,
                            <br />and let React update the UI to match them.</p>
                        <Link className="btn btn-success text-white card-link" to="/conditional-example-part1">Example</Link>  
                    </div>
                </div>
            </div>
            );
}


export {
Greeting,
        GreetingExample,
        }