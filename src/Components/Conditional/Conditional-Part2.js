import React, { Component } from 'react';
import { Link } from 'react-router-dom';

// Main component
class LoginControlExample extends Component {
    constructor(props) {
        super(props);
        this.handleLoginClick = this.handleLoginClick.bind(this);
        this.handleLogoutClick = this.handleLogoutClick.bind(this);
        this.state = {isLoggedIn: false};
    }

    handleLoginClick() {
        this.setState({isLoggedIn: true});
    }

    handleLogoutClick() {
        this.setState({isLoggedIn: false});
    }

    render() {
        const isLoggedIn = this.state.isLoggedIn;

        let button = null;
        if (isLoggedIn) {
            button = <LogoutButton onClick={this.handleLogoutClick} />;
        } else {
            button = <LoginButton onClick={this.handleLoginClick} />;
        }

        return (
                <div>
                    <Greeting isLoggedIn={isLoggedIn} />
                    {button}
                </div>
                );
    }
}

// Component for logged in user
function UserGreeting(props) {
    return <h1>Welcome back!</h1>;
}

// Component for the guest user
function GuestGreeting(props) {
    return <h1>Please sign up.</h1>;
}

// Greeting component to render another component based on the logged state
function Greeting(props) {
    const isLoggedIn = props.isLoggedIn;
    if (isLoggedIn) {
        return <UserGreeting />;
    }
    return <GuestGreeting />;
}

// Component to render a button according to the logged in state
function LoginButton(props) {
    return (
            <button className="btn btn-success" onClick={props.onClick}>
                Login
            </button>
            );
}

// Component to render a button according to the logged out state
function LogoutButton(props) {
    return (
            <button className="btn btn-warning" onClick={props.onClick}>
                Logout
            </button>
            );
}

// Component to render the UI
class LoginControl extends Component {
    render() {
        return(
                <div>
                    <div className="card text-left">
                        <div className="card-block">
                            <h4 className="card-title text-muted">Element Variables</h4>
                            <p className="card-text">
                                * You can use variables to store elements. This can help you conditionally render a part of the component while the rest of the output doesn’t change.
                                <br />* While declaring a variable and using an if statement is a fine way to conditionally render a component.</p>
                            <Link className="btn btn-success text-white card-link" to="/conditional-example-part2">Example</Link>  
                        </div>
                    </div>
                </div>
                );
    }
}

export { LoginControl,
        LoginControlExample,
        }