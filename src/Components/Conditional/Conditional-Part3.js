import React, { Component } from 'react';
import { Link } from 'react-router-dom';

// Main component
class WarningBannerExample extends Component{
  constructor(props) {
    super(props);
    this.state = {showWarning: true}
    this.handleToggleClick = this.handleToggleClick.bind(this);
  }
  
  // Method to set the value of warn property from its previous state
  handleToggleClick() {
    this.setState(prevState => ({
      showWarning: !prevState.showWarning
    }));
  }
  
  render() {
    return (
      <div>
        <WarningBanner warn={this.state.showWarning} /><br/>
        <button className={this.state.showWarning ? 'btn btn-success' : 'btn btn-primary'} onClick={this.handleToggleClick}>
          {this.state.showWarning ? 'Hide' : 'Show'}
        </button>
      </div>
    );
  }
}

// Component to render a red banner which when warn property is set to false returns null
function WarningBanner(props) {
  if (!props.warn) {
    return null;
  }

  return (
    <div className="warning">
      Warning!
    </div>
  );
}

// Component to render the UI
class Page extends Component {
    render(){
        return (
                <div>
                    <div className="card text-left">
                        <div className="card-block">
                            <h4 className="card-title text-muted">Preventing Component from Rendering</h4>
                            <p className="card-text">
                                * In rare cases you might want a component to hide itself even though it was rendered by another component. To do this return null instead of its render output.
                                <br />* Returning null from a component’s render method does not affect the firing of the component’s lifecycle methods.
                                <br />&nbsp;&nbsp;&nbsp;For instance, <b><i>componentWillUpdate</i></b> and <b><i>componentDidUpdate</i></b> will still be called.</p>
                            <Link className="btn btn-success text-white card-link" to="/conditional-example-part3">Example</Link>  
                        </div>
                    </div>
                </div>
                );
    }
}

export {
    Page,
    WarningBannerExample
}