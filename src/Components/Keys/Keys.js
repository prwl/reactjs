import React from 'react';
import { Link } from 'react-router-dom';

// Main Component
function NumberKeyExample(props){
  const liStyle = {
    listStylePosition: "inside",
}
  const numbers = props.numbers;
  const listItems = numbers.map((number) =>
    <li key={number.toString()}>
      {number}
    </li>
  );
  return (
    <ul style={liStyle}>{listItems}</ul>
  );
}

// Component to render the UI
function NumberKey(props) {
    return(
            <div>
                <div className="card text-left">
                    <div className="card-block">
                        <h4 className="card-title text-muted">Keys</h4>
                        <p className="card-text">
                            * A “key” is a special string attribute you need to include when creating lists of elements.
                            <br />* Keys help React identify which items have changed, are added, or are removed.
                            <br />* Keys should be given to the elements inside the array to give the elements a stable identity.
                            <br />* When you don’t have stable IDs for rendered items, you may use the item index as a key as a last resort.
                            <br />* It is not recommended using indexes for keys if the order of items may change. This can negatively impact performance and may cause issues with component state.
                            <br />* If you choose not to assign an explicit key to list items then React will default to using indexes as keys.</p>
                        <Link className="btn btn-success text-white card-link" to="/keys-example">Example</Link>  
                    </div>
                </div>
            </div>
            );
}

export {
    NumberKey,
    NumberKeyExample
}