import React from 'react';
import { Link } from 'react-router-dom';

// Main component
function BlogExample(props) {
    const liStyle = {
        listStylePosition: "inside",
    }
    const sidebar = (
            <ul style={liStyle}>
                {props.posts.map((post) =>
                                <li key={post.id}>
                                    {post.title}
                                </li>
                            )}
            </ul>
            );
    const content = props.posts.map((post) =>
        <div key={post.id}>
            <h3>{post.title}</h3>
            <p>{post.content}</p>
        </div>
    );
    return (
            <div>
                {sidebar}
                <hr />
                {content}
            </div>
            );
}

// Component to render the UI
function Blog(props) {
    return(
            <div>
                <div className="card text-left">
                    <div className="card-block">
                        <h4 className="card-title text-muted">Extracting Components with Keys</h4>
                        <p className="card-text">
                            * Keys only make sense in the context of the surrounding array.
                            <br />* A good rule of thumb is that elements inside the map() call need keys.</p>
                        <h5 className="card-subtitle mb-2 text-muted">Keys Must Only Be Unique Among Siblings</h5>
                        * However they don’t need to be globally unique. We can use the same keys when we produce two different arrays.
                        <br />* Keys serve as a hint to React but they don’t get passed to your components. If you need the same value in your component, pass it explicitly as a prop with a different name.
                        <br />* For Example:
                        <br />&nbsp;&nbsp;&nbsp; component can read props.id, but not props.key.<br /><br />
                        <Link className="btn btn-success text-white card-link" to="/keys-example-part2">Example</Link>  
                    </div>
                </div>
            </div>
            );
}

export { Blog,
        BlogExample
}