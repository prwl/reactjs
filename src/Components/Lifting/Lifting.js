import React, { Component } from 'react';
import { Link } from 'react-router-dom';

// Main component 
class CalculatorExample extends Component {
    constructor(props) {
        super(props);
        this.handleCelsiusChange = this.handleCelsiusChange.bind(this);
        this.handleFahrenheitChange = this.handleFahrenheitChange.bind(this);
        this.state = {temperature: '', scale: 'c'};
    }

    // Method to set the state of the scale and the temperature on user input
    handleCelsiusChange(temperature) { 
        this.setState({scale: 'c', temperature});
    }

    // Method to set the state of the scale and the temperature on user input
    handleFahrenheitChange(temperature) {
        this.setState({scale: 'f', temperature});
    }

    render() {
        const scale = this.state.scale;
        const temperature = this.state.temperature;
        const celsius = scale === 'f' ? tryConvert(temperature, toCelsius) : temperature;
        const fahrenheit = scale === 'c' ? tryConvert(temperature, toFahrenheit) : temperature;

        return (
                <div>
                    <TemperatureInput
                        scale="c"
                        temperature={celsius}
                        onTemperatureChange={this.handleCelsiusChange} />
                    <TemperatureInput
                        scale="f"
                        temperature={fahrenheit}
                        onTemperatureChange={this.handleFahrenheitChange} />
                    <BoilingVerdict
                        celsius={parseFloat(celsius)} />
                </div>
                );
    }
}

// Method to convert the temperature to celsius
function toCelsius(fahrenheit) {
    return (fahrenheit - 32) * 5 / 9;
}

// Method to convert the temperature to fahrenheit
function toFahrenheit(celsius) {
    return (celsius * 9 / 5) + 32;
}

// Method to call the above 2 methods with the temperature parameter to convert temperatures into their respective fields 
function tryConvert(temperature, convert) {
    const input = parseFloat(temperature);
    if (Number.isNaN(input)) {
        return '';
    }
    const output = convert(input);
    const rounded = Math.round(output * 1000) / 1000;
    return rounded.toString();
}

// Method to show message according to the condition of the temperature
function BoilingVerdict(props) {
    if (props.celsius >= 100) {
        return <p className="bg-success text-white">The water would boil.</p>;
    }
    return <p className="bg-danger text-white">The water would not boil.</p>;
}

const scaleNames = {
    c: 'Celsius',
    f: 'Fahrenheit'
};

// Component to render an input text field to enter the temperature
class TemperatureInput extends Component {
    constructor(props) {
        super(props);
        this.handleChange = this.handleChange.bind(this);
    }

    handleChange(e) { 
        this.props.onTemperatureChange(e.target.value);
    }

    render() {
        const temperature = this.props.temperature;
        const scale = this.props.scale;
        return (
                <div className="form-group row">
                    <label className="col-sm-2 col-form-label">Enter temperature in {scaleNames[scale]}:</label>
                    <input className="col-sm-2 form-control" value={temperature}
                           onChange={this.handleChange} />
                </div>
                );
    }
}

// Component to render the UI
class Calculator extends Component {
    render() {
        return(
                <div>
                    <div className="card text-left">
                        <div className="card-block">
                            <h4 className="card-title text-muted">Lifting State Up</h4>
                            <p className="card-text">
                                * Often, several components need to reflect the same changing data. We recommend lifting the shared state up to their closest common ancestor.
                                <br />* There should be a single “source of truth” for any data that changes in a React application. Usually, the state is first added to the component that needs it for rendering.
                                <br />* Then, if other components also need it, you can lift it up to their closest common ancestor. 
                                <br />* Instead of trying to sync the state between different components, you should rely on the top-down data flow.</p>
                            <Link className="btn btn-success text-white card-link" to="/lifting-example">Example</Link>  
                        </div>
                    </div>
                </div>
                );
    }
}

export {
Calculator,
        CalculatorExample
        }