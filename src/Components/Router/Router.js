import React from 'react';
        function App() {
        return (
<div>
    <blockquote className="blockquote">
        <p className="mb-0">Use npm to install react-router-dom.<br /><br />
            1. &lt;BrowserRouter&gt; --
            http://example.com/about
            <br />
            2. &lt;HashRouter&gt; --
            http://example.com/#/about</p>
    </blockquote>

    <blockquote className="blockquote">
        <p> The <b><i>&lt;Route&gt;</i></b> component is the most important component in React router. 
            It renders some UI if the current location matches the route’s path. 
            Ideally, a <b><i>&lt;Route&gt;</i></b> component should have a prop named path, and if the pathname is
            matched with the current location, it gets rendered.</p>
    </blockquote>

    <blockquote className="blockquote">
        <p> The <b><i>&lt;Link&gt;</i></b> component, on the other hand, is used to navigate between pages.
            It’s comparable to the HTML anchor element. 
            However, using anchor links would result in a browser refresh, 
            which we don’t want. So instead, we can use <b><i>&lt;Link&gt;</i></b> to navigate to a 
            particular URL and have the view re-rendered without a browser refresh.</p>
    </blockquote>
</div>
                );
                }

export default App;