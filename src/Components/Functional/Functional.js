import React from 'react';
import { Link } from 'react-router-dom';

// Main Component 
function Welcome(props) {
    return <div>
        <h1>Hello, {props.name}</h1></div>;
}

// Component to display the UI
function Functional(props) {
    return <div>
    <div className="card text-left">
        <div className="card-block">
            <h4 className="card-title text-muted">Components</h4>
            <p className="card-text">1. Components let you split the UI into independent,
                reusable pieces, and think about each piece in isolation.
                <br /> 2. Conceptually, components are like JavaScript functions. They accept arbitrary inputs (called “props”) 
                <br />and return React elements describing what should appear on the screen.</p>
            <h5 className="card-subtitle mb-2 text-muted">Functional Components</h5>
            <p>* The simplest way to define a component is to write a JavaScript function:<br />
                <br />The function in the example is a valid React component because it accepts a single<br /> 
                “props” (which stands for properties) object argument with data and returns a React element.<br /> 
                We call such components “functional” because they are literally JavaScript functions.</p>
            <Link className="btn btn-success text-white card-link" to="/welcome">Example</Link>  
        </div>
    </div>  
</div>
}

export {
Welcome,
        Functional,
        }