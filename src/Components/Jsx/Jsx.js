import React, { Component } from 'react';
import { Link } from 'react-router-dom';

// Main component class
class Jsx extends Component {
    render() {
        return (<div>
            {element}
        </div>
                );
    }
}

function formatName(user) {
    return user.firstName + ' ' + user.lastName;
}

const user = {
    firstName: 'Prawal',
    lastName: 'Sharma'
};

const element = (
        <div>
            <h1>
                Hello, {formatName(user)}!
            </h1>
        </div>
        );


// Class to show the UI and a button named example
class JsxExample extends Component {
    render() {
        return (
                <div>
                    <div className="card text-left">
                        <div className="card-block">
                            <h4 className="card-title text-muted">Jsx</h4>
                            <p className="card-text"><span className="d-inline bg-inverse text-white">{'const element = <h1>Hello, world!</h1>;'}</span>
                                <br />* This funny tag syntax is neither a string nor HTML.
                                <br />* It is called JSX, and it is a syntax extension to JavaScript.</p>
                            <blockquote className='blockquote bg-warning text-white'>
                                Warning:
                                Since JSX is closer to JavaScript than to HTML, React DOM uses camelCase property naming convention instead of HTML attribute names.
                                For example, class becomes className in JSX, and tabindex becomes tabIndex.</blockquote>
                
                            <blockquote className='blockquote bg-inverse text-white'>
                                Fundamentally, JSX just provides syntactic sugar for the: 
                                <br /><b><i>React.createElement(component, props, ...children)</i></b> function. The JSX code:
                                <br /></blockquote>
                            <blockquote className='blockquote bg-info text-white'>
                                &lt;MyButton color="blue" shadowSize={2}&gt;
                                <br />&nbsp;&nbsp;Click Me
                                <br />&lt;/MyButton&gt;
                            </blockquote>
                            compiles into:
                            <br /><br />
                            <blockquote className='blockquote bg-info text-white'>
                                React.createElement(
                                <br />MyButton,
                                <br />{'{color: "blue", shadowSize: 2},'}
                                <br />'Click Me'
                                <br />) 
                            </blockquote>
                            You can also use the self-closing form of the tag if there are no children. So:
                            <blockquote className='blockquote bg-info text-white'>
                                &lt;div className="sidebar" /&gt;
                            </blockquote>compiles into:
                            <br /><br /><blockquote className='blockquote bg-info text-white'>
                                React.createElement(
                                <br />'div',
                                <br />{'{className: "sidebar"},'}
                                <br />null
                                <br />)
                            </blockquote>
                            <br /><Link className="btn btn-success text-white card-link" to="/jsx-example">Example</Link>  
                        </div>
                    </div>
                </div>
                );
    }
};

export {
Jsx,
        JsxExample
        }