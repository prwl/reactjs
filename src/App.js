import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import View from './Main';


    const linkStyle = {
        textDecoration: "none"
    }
    
class App extends Component {
    render() {
        return (
                <div className="bs-example">
                    <div className="bg-primary text-center"><Link style={linkStyle} className="text-white display-4" to="/">React Js</Link></div>
                    <table className="table table-striped table-bordered table-hover">
                        <tbody>
                            <tr>
                                <td><Link to="/functional">Functional Components</Link></td>
                                <td><Link to="/class">Class Components</Link></td>
                                <td><Link to="/jsx">JSX</Link></td>
                                <td><Link to="/rendering-elements">Rendering Elements</Link></td>
                                <td><Link to="/handling">Handling Events</Link></td>
                                <td><Link to="/conditional">Conditional Rendering</Link></td>
                            </tr>
                            <tr>
                                <td><Link to="/conditional-part2">Conditional Render. Exam.2</Link></td>
                                <td><Link to="/conditional-part3">Conditional Render. Exam.3</Link></td>
                                <td><Link to="/list">List</Link></td>
                                <td><Link to="/keys">Keys</Link></td>
                                <td><Link to="/keys-part2">Keys Exam.2</Link></td>
                                <td><Link to="/form">Form</Link></td>
                            </tr>
                            <tr>
                                <td><Link to="/controlled-components">Controlled Components</Link></td>
                                <td><Link to="/lifting">Lifting state up</Link></td>
                                <td><Link to="/composition">Composition vs Inheritance</Link></td>
                                <td><Link to="/composition-part2">Composition Example 2</Link></td>
                                <td><Link to="/composition-part3">Composition Example 3</Link></td>
                                <td><Link to="/router">Router</Link></td>
                            </tr>
                        </tbody>
                    </table>
                
                    <View />
                
                </div>
                );
    }
}

export default App;
